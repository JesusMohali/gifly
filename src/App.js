import React from 'react';
import './App.css';
import Home from './pages/Home/index'
import { Link, Route } from 'wouter'
import logo from './assets/logo.png'
import Details from './pages/Details';
import SearchResults from './pages/SearchResults';
import StaticContext from './context/StaticContext'
import { GifsContextProvider } from './context/GifsContext';

function App() {
  return (
    <StaticContext.Provider value={2}>
      <div className="App">
        <section className="App-content">
          <Link to='/'>
            <div className='align-title'>
              <img className='App-logo' src={logo} alt='logo' />
              <h1>Gifly</h1>
            </div>
          </Link>
          <GifsContextProvider>
            <Route
              component={Home}
              path="/" />
            <Route
              component={SearchResults}
              path="/search/:keyword" />
            <Route
              component={Details}
              path="/gif/:id" />
          </GifsContextProvider>
        </section>
      </div>
    </StaticContext.Provider>
  );
}

export default App;
