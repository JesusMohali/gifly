import React from "react";
import { Link } from "wouter"
import './Category.css'

export default function Category({ name, options = [] }) {
    return <>
        <h3 className="App-title">{name}</h3>
        <ul>
            {options.map((popularGifs) => (
                <li key={popularGifs}>
                    <Link to={`/search/${popularGifs}`}> {popularGifs} </Link>
                </li>
            ))}
        </ul>
    </>
}