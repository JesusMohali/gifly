import React from "react";
import './Gif.css'
import { Link } from 'wouter'

function Gif({ title, id, url }) {
    return (
        <div className="gif_item">
            <Link to={`/gif/${id}`}>
                <img src={url} alt={title} className="gif_img" />
                <h6 className="gif_title">{title}</h6>
            </Link>
        </div>
    )
}
export default React.memo(Gif)