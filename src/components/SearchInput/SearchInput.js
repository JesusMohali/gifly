import React, { useState } from "react";
import { useLocation } from "wouter";
import './styles.css'

function SearchInput() {

    const [keyword, setKeyword] = useState('')
    const [path, pushLocation] = useLocation()
    
    const handleSubmit = evt => {
        evt.preventDefault()
        pushLocation(`/search/${keyword}`)
    }
    const handleChange = evt => {
        setKeyword(evt.target.value)
    }
    return (
        <>
            <form onSubmit={handleSubmit} className="form-class">
                <input placeholder="Search a gif here..." onChange={handleChange} type="search" value={keyword} />
                <button onSubmit={handleSubmit}>Buscar</button>
            </form>
        </>
    )
}

export default React.memo(SearchInput)