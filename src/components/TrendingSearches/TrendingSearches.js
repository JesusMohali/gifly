import React, { useEffect, useState } from "react";
import getTrendingTerms from "../../services/getTrendingTermsService.";
import Category from '../../components/Category/index'

export default function TrendingSearches() {
    const [trending, setTrending] = useState([])

    useEffect(() => {
        getTrendingTerms().then(setTrending)
    }, [])

    return <Category name='Tendencias' options={trending} />
}