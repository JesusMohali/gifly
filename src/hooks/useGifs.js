import { useContext, useEffect, useState } from "react"
import getGifs from "../services/getGifs"
import GifsContext from '../context/GifsContext'

const INITIAL_PAGE = 0

export function useGifs({ keyword } = { keyword: null }) {
    const [loading, setLoading] = useState(false)
    const [loadingNextPage, setLoadingNextPage] = useState(false)
    const [page, setPage] = useState(INITIAL_PAGE)
    const { gifs, setGifs } = useContext(GifsContext)

    const keywordToUse = keyword || localStorage.getItem('lastKeyword') || 'coding'

    useEffect(function () {
        setLoading(true)

        getGifs({ keyword: keywordToUse })
            .then(res => {
                setGifs(res)
                setLoading(false)
                //Save the keyword in localStorage
                localStorage.setItem('lastKeyword', keywordToUse)
            })
    }, [keyword, keywordToUse, setGifs])

    useEffect(function () {
        if (page === INITIAL_PAGE) return
        setLoadingNextPage(true)

        getGifs({ keyword: keywordToUse, page })
            .then(nextGifs => {
                setGifs(prevGifs => prevGifs.concat(nextGifs))
                setLoadingNextPage(false)
            })

    }, [keywordToUse, page, setGifs])

    return { loading, loadingNextPage, gifs, setPage }
}