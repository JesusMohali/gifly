import React from "react";
import Gif from "../../components/Gif/Gif";
import useGlobalGifs from "../../hooks/useGlobalGifs";

export default function Details({ params }) {
    const gifs = useGlobalGifs()

    const gif = gifs.find(singleGifs => singleGifs.id === params.id)

    return<>
        <h3 className="App-title">{gif.title}</h3>
        <Gif {...gif} />
    </>
}