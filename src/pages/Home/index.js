import React from "react";
import ListOfGifs from "../../components/ListOfGifs/ListOfGifs";
import Spinner from "../../components/Spinner";
import SearchInput from "../../components/SearchInput/SearchInput";
import { useGifs } from "../../hooks/useGifs";
import './styles.css'
import TrendingSearches from "../../components/TrendingSearches/index";

export default function Home() {
    const { loading, gifs } = useGifs()
    return (
        <>
            <SearchInput />
            <div className="container">
                <section className="lastGifs">
                    <h3 className="App-title">Última búsqueda</h3>
                    {loading
                        ? <Spinner />
                        : <ListOfGifs gifs={gifs} />
                    }
                </section>
                <section className="mostPopulars">
                    <TrendingSearches />
                </section>
            </div>

        </>

    )
}