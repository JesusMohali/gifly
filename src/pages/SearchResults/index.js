import React, { useCallback, useEffect, useRef } from "react"
import Spinner from "../../components/Spinner/index"
import ListOfGifs from "../../components/ListOfGifs/ListOfGifs"
import SearchInput from "../../components/SearchInput/SearchInput"
import { useGifs } from "../../hooks/useGifs"
import useNearScreen from "../../hooks/useNearScreen"
import debounce from 'just-debounce-it'

export default function SearchResults({ params }) {
    const { keyword } = params
    const { loading, gifs, setPage } = useGifs({ keyword })
    const externalRef = useRef()
    const { isNearScreen } = useNearScreen({
        externalRef: loading ? null : externalRef,
        once: false,
        distance: '200px'
    })
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const debounceHandleNextPage = useCallback(debounce(
        () => setPage(prevPage => prevPage + 1), 200
    ), [setPage])

    useEffect(function () {
        if (isNearScreen) debounceHandleNextPage()
    }, [isNearScreen, debounceHandleNextPage])

    return <>
        <SearchInput />
        {loading
            ? <Spinner />
            : <>
                <h3 className="App-title">{decodeURI(keyword)}</h3>
                {gifs.length === 0
                ? <h3 className="App-title">No hay resultados</h3>
                : <ListOfGifs gifs={gifs} />
                }
            </>
        }
        <div id="visor" ref={externalRef}></div>
    </>

}